# Create AWS container registry

terraform {
  required_version = ">= 0.11.8"
}

provider "aws" {
  version = ">= 2.6.0"
  region  = "${var.region}"
}

resource "aws_ecr_repository" "divvydose" {
  name = "divvydose_hello"
}

output "repo_registry_id" {
  value = "${aws_ecr_repository.divvydose.registry_id}"
}

output "repo_url" {
  value = "${aws_ecr_repository.divvydose.repository_url}"
}


