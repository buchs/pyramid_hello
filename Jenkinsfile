#!/usr/bin/env groovy

/* --------------------------------------------------------
This Jenkins Declarative Pipeline illustrates a solution to CI/CD
for a simple Python app deployed in containers. Before this pipeline
is executed, the following things need to be done to prepare the
Jenkins infrastructure and configuration as necessary.
  1. See setup-workstation for some applications that need to be
     installed on Jenkins workers
  2. cd kube_role; terraform init; terraform apply -auto-approve
     # this creates a IAM role that can be used by Kubernetes
     # workers. It is a 1-time operation per AWS account.
  3. cd ../container_reg; terraform init; terraform apply \
     -auto-approve
     # creates AWS ECR container repository
  4. cd ../kube_cluster; terraform init; terraform apply \
     -auto-approve
     # creates kubernetes cluster with AWS EKS service
  5. ./post-terraform.sh
     # configuration for kubernetes - application, service


Triggering the Jenkins job of this pipeline:
I chose GitLab for this exercise just to have the opportunity to
learn more about it. I assume that a GitLab webhook to trigger
Jenkins pipeline has been set up. We need to evaluate the tag to
determine if it matches either of these regexps:
"test_v[0-9]+\.[0-9]+\.[0-9]+" or "prod_v[0-9]+\.[0-9]+\.[0-9]+".
If it matches the first then deploy to test environment and if
it matches second, then deploy to prod environment. If neither,
then silently abort. The code to perform this operation is yet to
be created. The documentation on GitLab sounds like this might be
difficult to do. This page https://docs.gitlab.com/ee/user/project/
integrations/webhooks.html#webhook-endpoint-tips
shows the request body delivered on a tag push hook. Field of
interest is "ref". It might be the Generic Webhook Trigger can
capture what is required.

Jenkins Plugins required:
  *Pipeline - typical ones required for Pipelines*
  Git
  GitLab
  Generic Webhook Trigger
  
----------------------------------------------------------- */

pipeline {
  agent any
  stages {

    stage('Setup') {
      steps {
        script {
          // somehow, we get the tag...
          // compare the tag against these regexps
          def match_test = ( tag =~ /test_v[0-9]+\.[0-9]+\.[0-9]+/ )
          def match_prod = ( tag =~ /prod_v[0-9]+\.[0-9]+\.[0-9]+/ )
	  if (!match_test && !match_prod) {
	     currentBuild.result = 'ABORTED'
	     error('Stopping, because some other tag was involved')
	  }
          if (match_test) {
	    def DEPLOY_TO = "test"
          }
	  if (match_prod) {
	    def DEPLOY_TO = "prod"
	  }
          // extract the version string from the tag using regexp:
          def match_ver = ( tag =~ /(test|prod)_(v[0-9]+\.[0-9]+\.[0-9]+)/ )
          def version = match_ver[0][2]
	}
      }
    }
    
    stage('SCM') {
      steps {
         // clone master branch from gitlab
         git('https://gitlab.com/buchs/pyramid_hello.git')
      }
    }

    stage('LocalTestingApp') {
      steps {
        sh '''
	  cd app; pip3.6 install -r requirements.txt;
          python3.6 ./main.py & ; 
          curl -s http://localhost:6543 | grep -q 'Hello World!'
          if [ $? -ne 0 ]; then
            exit 1
          fi
          kill -9 %1
	'''
      }
    }

    stage('BuildContainer') {
      steps {
        sh 'docker build -t divvydose_hello:$version  .'
      }
    }

    stage('LocalTestingContainer') {
      steps {
        sh '''
           // test container locally
           containerId=$(docker run --rm -d -p 80:6543 divvydose_hello:$version)
           curl -s http://localhost:80 | grep -q 'Hello World!'
           if [ $? -ne 0 ]; then
             exit 1
           fi
           docker kill $containerId
	'''
      }
    }

    stage('PublishContainer') {
      steps {
        // publish container to AWS ECR registry
        sh '''
	   # 1. login to registry
           $(aws ecr get-login --no-include-email --region us-east-2)
           # 2. get hostname of repository
           ECR_HOST="$(aws ecr describe-repositories  \
              --repository-names divvydose_hello      \
              --query 'repositories[0].repositoryUri'  \
              --output text | \
             sed 's?/divvydose_hello??')"
           # tag image for ECR, and push
           docker tag divvydose_hello:$version   \
	      $ECR_HOST/divvydose_hello:$version
           docker push $ECR_HOST/divvydose_hello:$version
	'''
      }
    }

    stage('DeployTest') {
      /* This section assumes that the full environment for the kube
         test cluster is dynamically built as required. So, we have
	 to run terraform, etc. We simply leave this environment running,
	 with the assumption that when the test environment is done being
	 used, someone will destroy it via terraform. */
      when {
        beforeAgent true
        environment name: 'DEPLOY_TO', value: 'prod'
      }
      steps {
         sh '''
	    cd kube_cluster
	    terraform -var environment=test -state=terraform-test.tfstate \
	      init
	    terraform -var environment=test -state=terraform-test.tfstate \
	      apply -auto-approve
	    ./post-terraform.sh test
            ECR_HOST="$(aws ecr describe-repositories  \
               --repository-names divvydose_hello      \
               --query 'repositories[0].repositoryUri'  \
               --output text | \
              sed 's?/divvydose_hello??')"
	    image=$ECR_HOST/divvydose_hello:$version

            kubectl --kubeconfig kube_test_config.yml set image \
	      deployments/hello-world-test=$image
	    cd ..
	 '''
      }
    }

    stage('DeployProd') {
      when {
        beforeAgent true
        environment name: 'DEPLOY_TO', value: 'prod'
      }
      steps {
      	 sh '''
	    cd kube_cluster
            ECR_HOST="$(aws ecr describe-repositories  \
               --repository-names divvydose_hello      \
               --query 'repositories[0].repositoryUri'  \
               --output text | \
              sed 's?/divvydose_hello??')"
	    image=$ECR_HOST/divvydose_hello:$version

            kubectl --kubeconfig kube_config.yml set image \
	      deployments/hello-world-prod=$image
	    cd ..
	 '''
      }
    }

  }
}
