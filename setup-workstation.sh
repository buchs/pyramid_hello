
# Assuming Linux, we need a couple of applications on your build platform

# need python3.6 and pip3.6
# left as exercise for the reader - depends upon GNU/Linux distribution

# aws cli
pip3 install awscli --upgrade


# aws eks authenticator
cd kube_cluster
url="https://amazon-eks.s3-us-west-2.amazonaws.com/1.12.7/2019-03-27/"
url="${url}bin/linux/amd64/aws-iam-authenticator"
curl -o aws-iam-authenticator $url
cd ..

# kubectl
# find latest stable version
url="https://storage.googleapis.com/kubernetes-release/release/stable.txt"
version=$(curl -s $url)
# retrieve that version
url="https://storage.googleapis.com/kubernetes-release/release/"
url="${url}${version}/bin/linux/amd64/kubectl"
curl -LO $url

# other stuff
echo "Remember to configure aws cli credentials (~/.aws/credentials)"

