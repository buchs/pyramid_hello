FROM python:3.6-slim

ADD app /opt/app/

RUN apt-get update; cd /opt/app; /usr/local/bin/pip3.6 install -r requirements.txt

EXPOSE 6543

ENTRYPOINT [ "/usr/local/bin/python3.6", "/opt/app/main.py" ]
