output "role_name" {
  description = "the name of the created role"
  value       = "${aws_iam_role.eks_service_role.name}"
}
