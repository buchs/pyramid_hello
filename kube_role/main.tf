# One-time operation for each AWS account, to set up the proper EKS role

terraform {
  required_version = ">= 0.11.8"
}


provider "aws" {
  version = ">= 2.6.0"
  region  = "${var.region}"
}


resource "aws_iam_role" "eks_service_role" {
  name = "eks_service_role"
  assume_role_policy = <<EOB
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOB
}


resource "aws_iam_role_policy_attachment" "attached_policy_1" {
  role = "${aws_iam_role.eks_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
}

resource "aws_iam_role_policy_attachment" "attached_policy_2" {
  role = "${aws_iam_role.eks_service_role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

