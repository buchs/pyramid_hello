variable "environment" {
  describe = "Name of the destined environment, prod or test"
  default = "prod"
}

variable "region" {
  default = "us-east-2"
}

