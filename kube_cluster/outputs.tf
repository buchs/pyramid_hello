output "EKS endpoint" {
  value = "${module.eks.cluster_endpoint}"
}


output "kube_ca_data" {
  value = "${module.eks.cluster_certificate_authority_data}"
}


output "kube_config" {
  value = "${module.eks.kubeconfig}"
}
