#!/usr/bin/env bash

# if a command line argument is present, then this is run for the test
# environment, else for prod

if [ $# -gt 1 ]; then
  kind=test
  kube_file=kube_test_config.yml
  tfstate="-state=terraform-test.tfstate"
else
  kind=prod
  kube_file=kube_config.yml
  tfstate=""
fi

# save configuration for kubectl
terraform output "$tfstate" kube_config > $kube_file

# find the eks host name for the cluster
eks_host=$(grep --color=no '^    server: ' $kube_file |  \
	     sed 's?^    server: https://??')

# test out kubectl configuration
kubectl --kubeconfig $kube_file get svc

kubectl --kubeconfig $kube_file cluster-info


# set up for deployments
# run application - base version
kubectl run --generator=run-pod/v1 divvydose-hello  \
    --kubeconfig $kube_file  \
    --replicas=3 --labels="run=divvydose_hello_$kind"  \
    --image=$eks_host/divvydose_hello:0.0.1 \
    --port=6543

# create a service object - puts LB in front of cluster.
kubectl expose deployment hello-world-$kind --type=NodePort \
    --kubeconfig $kube_file --name=divvydose-hello-$kind

#service has:
#NodePort:                 <unset>  31572/TCP
#Endpoints:                172.17.0.6:8080,172.17.0.7:8080,172.17.0.8:8080


