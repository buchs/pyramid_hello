
# Need the terraform-aws-eks module, found here:
# https://github.com/terraform-aws-modules/terraform-aws-eks.git

terraform {
  required_version = ">= 0.11.8"
}


provider "aws" {
  version = ">= 2.6.0"
  region  = "${var.region}"
}


provider "random" {
  version = "= 1.3.1"
}


resource "random_string" "suffix" {
  length  = 8
  special = false
}


locals {
  cluster_name = "divvydose-hello-${var.environment}-eks-${random_string.suffix.result}"
}


resource "aws_vpc" "vpc" {

  cidr_block = "10.0.0.0/22"

  tags = {
    Name = "eks-${var.environment}-vpc",
    "kubernetes.io/cluster/${local.cluster_name}" = "shared"
  }
}

resource "aws_subnet" "subnet_a" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.0.0/24"
}

resource "aws_subnet" "subnet_b" {
  vpc_id = "${aws_vpc.vpc.id}"
  cidr_block = "10.0.1.0/24"
}

resource "aws_subnet" "subnet_c" {
  cidr_block = "10.0.2.0/24"
  vpc_id = "${aws_vpc.vpc.id}"
}


data "aws_iam_role" "iam_role" {
  name = "eks_service_role"
}


module "eks" {
  version = "4.0.2"
  source       = "terraform-aws-modules/eks/aws"
  cluster_name = "${var.environment}-eks-cluster"
  subnets      = ["${aws_subnet.subnet_a.id}",
                   "${aws_subnet.subnet_b.id}",
                   "${aws_subnet.subnet_c.id}"]
  vpc_id       = "${aws_vpc.vpc.id}"
  
  # role_arn = "${data.aws_iam_role.iam_role.arn}"

  worker_groups = [
    {
      instance_type = "t3.small"
      asg_max_size  = 6
    }
  ]

  tags = {
    environment = "${var.environment}"
  }
}
